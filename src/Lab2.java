import java.util.Scanner;

public class Lab2 {
    public static void main(String[] args) {
        System.out.println("Please input your score");
        Scanner kbd=new Scanner(System.in);
        int score=kbd.nextInt();
        if(score>=80){
            System.out.println("You got grade A");
        }else if (score >=75){
            System.out.println("You got grade B+");
        }else if (score >=70){
            System.out.println("You got grade B");
        }else if (score >=65){
            System.out.println("You got grade C+");
        }else if (score >=60){
            System.out.println("You got grade C");
        }else if (score >=55){
            System.out.println("You got grade D+");
        }else if (score >=50){
            System.out.println("You got grade D");
        }else {
            System.out.println("You got grade F");
        }

    }
}
