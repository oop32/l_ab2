public class Javatypecasting1 {
    public static void main(String[] args) {
        //Widening casting เปลี่ยนข้อมูลขนาดเล็กไปเป็นขนาดใหญ่
        int myInt = 9;
        double myDouble = myInt; 
        System.out.println(myInt);      
        System.out.println(myDouble);   
    }
}
